import { loadDatabase } from '../src/providers/Database';
import Transactions from '../src/models/Transactions';
import User from '../src/models/User';
import Log from '../src/utils/logger';
import records from './records';

const getFromTo = async (from, to) => {
  const where = {};

  if (from && to) {
    where.value = [from, to];
  } else if (from && !to) {
    where.value = from;
  } else if (!from && to) {
    where.value = to;
  } else {
    return {
      success: false,
      data: {},
    };
  }
  Log.debug('getFromTo :: where :: %o', where);

  try {
    const userList = await User.find(where).lean().exec();

    if (!userList || userList.length === 0) {
      return {
        success: false,
        data: {},
      };
    }

    const data = {};

    if (from) {
      data.fromUser = userList.find((each) => each.value === from);
      if (!data.fromUser) {
        return {
          success: false,
          data: {},
        };
      }
    }

    if (to) {
      data.toUser = userList.find((each) => each.value === to);
      if (!data.toUser) {
        return {
          success: false,
          data: {},
        };
      }
    }

    return {
      success: true,
      data,
    };
  } catch (_err) {
    Log.error('allTransaction :: ', _err);
    return {
      success: false,
      data: {},
    };
  }
};

loadDatabase().then(async () => {
  const transactionData = [];
  const transactionCreatedData = [];
  try {
    records.map(async (each, index) => {
      Object.entries(each).map(async ([key, val]) => {
        if (val && val !== '') {
          transactionData.push({
            transactionOn: new Date(val),
            transactionType: 'debit',
            amount: 1000,
            method: 'deposit',
            from: key,
            to: 'cibi',
            deleted: false,
          });
        }
      });
      Log.info(`(index: ${`0${index + 1}`.slice(-2)} ) transaction added...`);
    });
    Log.info(`(transactionData: ${transactionData.length} ) All transaction data...`);
    const mapper = transactionData.map(async (transaction, j) => {
      const { success, data } = await getFromTo(transaction.from, transaction.to);
      if (success) {
        const { fromUser, toUser } = data;
        const createTransactions = new Transactions({ ...transaction,
          from: fromUser,
          to: toUser });
        return createTransactions.save().then(async (result) => {
          transactionCreatedData.push(result);
          Log.info(`(index: ${(`0${j + 1}`).slice(-2)} ) transaction created...`);
          if (transactionCreatedData.length === transactionData.length) {
            Log.info(`(transactionCreatedData: ${transactionCreatedData.length} ) All created transaction data... %o`, { });
            const find = await Transactions.find({}).exec();
            Log.info(`(find: ${find.length} ) All created transaction find data... %o`, { find });
            return process.exit(0);
          }
          return result;
        });
      }
      return transaction;
    });
    return await Promise.allSettled(mapper);
  } catch (_err) {
    Log.error('templateGenerate :: ', _err);
    return process.exit(1);
  }
});
