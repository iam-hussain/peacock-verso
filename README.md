# crew treasury
Concept of group of people fund manager
## Clone 

In terminal:   
```js   
$ git clone <repository clone url>
$ cd linqpal-task
```

## Server
Follow the below comments to run the server

### Server setup
```js   
cd server

// In case of running in Mac or Ubuntu Machine
npm run bootstrap 

// or

// In case of running in Windows Machine
npm run bootstrap-win

// or (manual setup)

npm i
npm run generate
npm run build
// Copy the example.env to .env

```
### Env setup
Add mongoDB user in .env
after coping the example.env to .env
``` js
$ nano .env 

MONGO_DB_URL=""
// past your mongoDB user in the variable of .env file
```
### Start the server

``` js
$ npm run start

// Server is running at 5000 port (can see in the log)
```