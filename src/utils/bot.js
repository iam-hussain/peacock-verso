import Log from './logger';
import { DISCORD_BOT_ID, USE_DISCORD_USERS_IN_OPTIONS } from '../providers/Configs';
import { AVAILABLE_COMMANDS, AVAILABLE_OTHER_COMMANDS, COMMANDS_KEY, OTHER_COMMANDS_KEY, OPTIONS_KEY } from './commands';
import handleDiscordActionCommands from '../controllers/discordController';

const commandsNames = AVAILABLE_COMMANDS.map((e) => (` "/${e}" `)).join(', ');
const botId = `<@${DISCORD_BOT_ID}>`;

export const handleBotMessage = (message) => {
  Log.debug('DISCORD_CLIENT :: handleBotMessage/message %o', { content: message.content });
  const formMessage = (input) => `${botId} ${input}`;
  if (message?.content.startsWith(botId)) {
    if (message.content.toLowerCase() === formMessage('help')) {
      message.reply(`Available commands: [${commandsNames}]`);
    } else {
      message.reply(`How can I help you <@${message.author.id}>? (try: @Peacock help)`);
    }
  }
};

const getOptions = (commandName, options) => {
  if (commandName === COMMANDS_KEY.delete_transaction) {
    return {
      transactionId: options.getString(OPTIONS_KEY.transactionId),
    };
  }

  const extractedOptions = {
    amount: options.getNumber(OPTIONS_KEY.amount),
    notes: options.getString(OPTIONS_KEY.notes),
  };
  const getUser = (key) => (USE_DISCORD_USERS_IN_OPTIONS
    ? options.getUser(key)
    : options.getString(key));

  if (commandName === COMMANDS_KEY.transfer) {
    extractedOptions.from = getUser(OPTIONS_KEY.from);
    extractedOptions.to = getUser(OPTIONS_KEY.to);
  } else if (commandName === COMMANDS_KEY.deposit) {
    extractedOptions.from = getUser(OPTIONS_KEY.member);
    extractedOptions.to = getUser(OPTIONS_KEY.treasurer);
  } else if (commandName === COMMANDS_KEY.withdraw) {
    extractedOptions.from = getUser(OPTIONS_KEY.treasurer);
    extractedOptions.to = getUser(OPTIONS_KEY.member);
  } else if (commandName === COMMANDS_KEY.expenditure) {
    extractedOptions.from = getUser(OPTIONS_KEY.treasurer);
  } else if (commandName === COMMANDS_KEY.invest) {
    extractedOptions.from = getUser(OPTIONS_KEY.treasurer);
    extractedOptions.to = getUser(OPTIONS_KEY.vendor);
  } else if (commandName === COMMANDS_KEY.return_on_invest) {
    extractedOptions.from = getUser(OPTIONS_KEY.vendor);
    extractedOptions.to = getUser(OPTIONS_KEY.treasurer);
  }

  return extractedOptions;
};

export const handleInteraction = async (interaction) => {
  Log.debug('DISCORD_CLIENT :: handleInteraction/commandName %o', { commandName: interaction.commandName });
  const { commandName, options } = interaction;
  if (![...AVAILABLE_OTHER_COMMANDS, ...AVAILABLE_COMMANDS].includes(commandName)) {
    return;
  }
  if (commandName === OTHER_COMMANDS_KEY.ping) {
    interaction.reply({
      content: 'Pong! **60ms**',
      ephemeral: true,
    });
    return;
  } if (commandName === OTHER_COMMANDS_KEY.help) {
    interaction.reply({
      content: `Available commands: [${commandsNames}]`,
      ephemeral: true,
    });
    return;
  }
  const { amount, from, to, notes, transactionId } = getOptions(commandName, options);
  // Log.debug('DISCORD_CLIENT :: handleInteraction/getOptions %o', { amount, from, to, notes });

  const replyMessage = await handleDiscordActionCommands(
    commandName,
    amount,
    from,
    to,
    notes,
    transactionId,
  );
  interaction.reply(replyMessage);
};
