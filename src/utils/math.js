import generator from 'generate-password';

export const randomGenerator = (size) => generator.generate({
  length: size,
  uppercase: true,
  numbers: true,
  exclude: true,
  excludeSimilarCharacters: true,
});

export const randomAlphabetsGenerator = (size) => generator.generate({
  length: size,
  uppercase: true,
  numbers: false,
  exclude: true,
  excludeSimilarCharacters: true,
});

export const randomNumberGenerator = (size) => generator.generate({
  length: size,
  uppercase: false,
  numbers: true,
  exclude: false,
  excludeSimilarCharacters: true,
});
