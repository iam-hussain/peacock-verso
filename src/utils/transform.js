import moment from 'moment-timezone';

moment.tz.setDefault("Asia/Calcutta");

export const COMMANDS_KEY = {
  transfer: 'Transferred',
  deposit: 'Deposited',
  withdraw: 'Withdraw',
  expenditure: 'Expenditure',
  invest: 'Investment',
  return_on_invest: 'Investment returns',
};

const dateFormate = "DD/MM/YYYY - hh:mm:ss A";
const dayFormate = 'DD-MMM-YYYY';

export const transformUserData = (user) => {
  const newDate = {
    ...user,
  };
  newDate.joinedDay = moment(user.joined).format(dayFormate);
  newDate.joinedDate = moment(user.joined).format(dateFormate);

  return newDate;
};

export const mapUserTransform = (data) => data.map((each) => transformUserData(each));

const transformTransaction = (transaction) => {
  const newDate = {
    ...transaction,
  };
  newDate.transactionDay = moment(transaction.transactionOn).format(dayFormate);
  // newDate.transactionDate = moment(transaction.transactionOn).format(dateFormate);
  newDate.createdDay = moment(transaction.createdAt).format(dayFormate);
  // newDate.createdDate = moment(transaction.createdAt).format(dateFormate);
  // newDate.updatedDay = moment(transaction.updatedAt).format(dayFormate);
  // newDate.updatedDate = moment(transaction.updatedAt).format(dateFormate);
  return newDate;
};

export const transformToTable = (data) => data.map((each) => {
  const newDate = transformTransaction(each);
  newDate.methodShow = COMMANDS_KEY[each.method];

  return newDate;
});

const mapTreasury = (data) => data.map((each) => transformUserData(each));

export const transformClubTreasury = (data) => {
  const newDate = {
    club: data.find((e) => e.value === "club"),
    members: mapTreasury(data.filter((e) => e?.user?.role === "member")),
    vendors: mapTreasury(data.filter((e) => e?.user?.role === "vendor")),
  };
  return newDate;
};