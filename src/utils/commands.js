import { Constants } from 'discord.js';
import { USE_DISCORD_USERS_IN_OPTIONS } from '../providers/Configs';
import { usersDetails, vendorsDetails } from '../providers/Constants';

const getOptions = (details) => details.map(({ name, value }) => ({ name, value }));

export const COMMANDS_KEY = {
  transfer: 'transfer',
  deposit: 'deposit',
  withdraw: 'withdraw',
  expenditure: 'expenditure',
  invest: 'invest',
  return_on_invest: 'return_on_invest',
  delete_transaction: 'delete_transaction',
};

export const OTHER_COMMANDS_KEY = {
  ping: 'ping',
  help: 'help',
};

export const AVAILABLE_COMMANDS = Object.values(COMMANDS_KEY).map((e) => e);
export const AVAILABLE_OTHER_COMMANDS = Object.values(OTHER_COMMANDS_KEY).map((e) => e);

export const OPTIONS_KEY = {
  amount: 'amount',
  notes: 'notes',
  from: 'from',
  to: 'to',
  member: 'member',
  vendor: 'vendor',
  treasurer: 'treasurer',
  transactionId: 'transaction_id',
};

const vendorChoice = getOptions([vendorsDetails[0]]);

const vendorReturnChoice = getOptions(vendorsDetails);

const userChoice = getOptions(usersDetails);

const userSettings = USE_DISCORD_USERS_IN_OPTIONS ? {
  type: Constants.ApplicationCommandOptionTypes.USER,
} : {
  type: Constants.ApplicationCommandOptionTypes.STRING,
  choices: userChoice,
};

const commands = [
  {
    name: OTHER_COMMANDS_KEY.ping,
    description: 'Bot uptime/latency checker.',
  },
  {
    name: OTHER_COMMANDS_KEY.help,
    description: 'Bot helper',
  },
  {
    name: COMMANDS_KEY.transfer,
    description: 'Transfer club money from one treasurer to another treasurer',
    options: [
      {
        name: OPTIONS_KEY.from,
        description: 'Transfer club money from a treasurer',
        required: true,
        ...userSettings,
      },
      {
        name: OPTIONS_KEY.amount,
        description: 'Money to be processed in rupees',
        required: true,
        type: Constants.ApplicationCommandOptionTypes.NUMBER,
      },
      {
        name: OPTIONS_KEY.to,
        description: 'Transfer club money to a treasurer',
        required: true,
        ...userSettings,
      },
      {
        name: OPTIONS_KEY.notes,
        description: 'Optional notes',
        required: false,
        type: Constants.ApplicationCommandOptionTypes.STRING,
      },
    ],
  },
  {
    name: COMMANDS_KEY.deposit,
    description: 'Deposit members money to club treasury by sending to a club treasurer',
    options: [
      {
        name: OPTIONS_KEY.member,
        description: 'The member who want to deposit',
        required: true,
        ...userSettings,
      },
      {
        name: OPTIONS_KEY.amount,
        description: 'Money to be processed in rupees',
        required: true,
        type: Constants.ApplicationCommandOptionTypes.NUMBER,
      },
      {
        name: OPTIONS_KEY.treasurer,
        description: 'The treasurer/member who collects the money',
        required: true,
        ...userSettings,
      },
      {
        name: OPTIONS_KEY.notes,
        description: 'Optional notes',
        required: false,
        type: Constants.ApplicationCommandOptionTypes.STRING,
      },
    ],
  },
  {
    name: COMMANDS_KEY.withdraw,
    description: 'Withdraw a member money from the club treasury',
    options: [
      {
        name: OPTIONS_KEY.member,
        description: 'The member who want to withdraw',
        required: true,
        ...userSettings,
      },
      {
        name: OPTIONS_KEY.amount,
        description: 'Money to be processed in rupees',
        required: true,
        type: Constants.ApplicationCommandOptionTypes.NUMBER,
      },
      {
        name: OPTIONS_KEY.treasurer,
        description: 'The treasurer/member who is paying',
        required: true,
        ...userSettings,
      },
      {
        name: OPTIONS_KEY.notes,
        description: 'Optional notes',
        required: false,
        type: Constants.ApplicationCommandOptionTypes.STRING,
      },
    ],
  },
  {
    name: COMMANDS_KEY.expenditure,
    description: 'Add expenditure of the club',
    options: [
      {
        name: OPTIONS_KEY.treasurer,
        description: 'The treasurer/member who is paying',
        required: true,
        ...userSettings,
      },
      {
        name: OPTIONS_KEY.amount,
        description: 'Money to be processed in rupees',
        required: true,
        type: Constants.ApplicationCommandOptionTypes.NUMBER,
      },
      {
        name: OPTIONS_KEY.notes,
        description: 'Optional notes, What expenditure is this?',
        required: false,
        type: Constants.ApplicationCommandOptionTypes.STRING,
      },
    ],
  },
  {
    name: COMMANDS_KEY.invest,
    description: 'Add investment of the club',
    options: [
      {
        name: OPTIONS_KEY.treasurer,
        description: 'The treasurer/member who is paying',
        required: true,
        ...userSettings,
      },
      {
        name: OPTIONS_KEY.amount,
        description: 'Money to be processed in rupees',
        required: true,
        type: Constants.ApplicationCommandOptionTypes.NUMBER,
      },
      {
        name: OPTIONS_KEY.vendor,
        description: 'Select the vendor you invested',
        required: true,
        type: Constants.ApplicationCommandOptionTypes.STRING,
        choices: vendorChoice,
      },
      {
        name: OPTIONS_KEY.notes,
        description: 'Optional notes',
        required: false,
        type: Constants.ApplicationCommandOptionTypes.STRING,
      },
    ],
  },
  {
    name: COMMANDS_KEY.return_on_invest,
    description: 'Add expenditure of the club',
    options: [
      {
        name: OPTIONS_KEY.vendor,
        description: 'Select the return on invest vendor',
        type: Constants.ApplicationCommandOptionTypes.STRING,
        required: true,
        choices: vendorReturnChoice,
      },
      {
        name: OPTIONS_KEY.amount,
        description: 'Money to be processed in rupees',
        required: true,
        type: Constants.ApplicationCommandOptionTypes.NUMBER,
      },
      {
        name: OPTIONS_KEY.treasurer,
        description: 'The treasurer/member who collects the money',
        required: true,
        ...userSettings,
      },
      {
        name: OPTIONS_KEY.notes,
        description: 'Optional notes',
        required: false,
        type: Constants.ApplicationCommandOptionTypes.STRING,
      },
    ],
  },
  {
    name: COMMANDS_KEY.delete_transaction,
    description: 'Delete a transaction with ID',
    options: [
      {
        name: OPTIONS_KEY.transactionId,
        description: 'Transaction ID generated while creating the transaction',
        required: true,
        type: Constants.ApplicationCommandOptionTypes.STRING,
      },
    ],
  },
];

export default commands;
