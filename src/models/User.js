import mongoose from '../providers/Database';

// Define the User Schema
export const userSchema = new mongoose.Schema(
  {
    value: { type: String },
    name: { type: String },
    description: { type: String },
    phone: { type: String },
    image: { type: String },
    joined: { type: Date, default: new Date() },
    role: { type: String, enum: ['member', 'vendor'], default: 'member' },
  },
  { timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  },
);

userSchema.virtual('treasury', {
  ref: 'Treasury',
  localField: '_id',
  foreignField: 'user',
  justOne: true,
});

userSchema.virtual('transaction_in', {
  ref: 'Transaction',
  localField: '_id',
  foreignField: 'to',
  match: { deleted: false },
});

userSchema.virtual('transaction_out', {
  ref: 'Transaction',
  localField: '_id',
  foreignField: 'from',
  match: { deleted: false },
});

userSchema.virtual('deposit_count', {
  ref: 'Transaction',
  localField: '_id',
  foreignField: 'from',
  count: true,
  match: { deleted: false, method: 'deposit' },
});

export default mongoose.model('User', userSchema, 'users');
