import mongoose from '../providers/Database';

// Define the Treasury Schema
export const treasurySchema = new mongoose.Schema(
  {
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    holding_amount: { type: Number, default: 0 }, // Liquidity
    spent_amount: { type: Number, default: 0 }, // Expenditure
    investment: { type: Number, default: 0 }, // Invested
    investment_return: { type: Number, default: 0 }, // Returns
    received: { type: Number, default: 0 }, // Collected
    number_of_paid_months: { type: Number, default: 0 },
    value: { type: String },
  },
  { timestamps: true },
);

export default mongoose.model('Treasury', treasurySchema, 'treasury');
