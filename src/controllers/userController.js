import Log from "../utils/logger";
import { successRespond, errorRespond, SHORT_MSG } from "../utils/responder";
import {
  oneUser,
  allUsers,
  allUsersOnly,
  deleteOneUser,
} from "../service/user";
import { transformUserData, mapUserTransform } from "../utils/transform";

export const deleteUserController = async (req, res) => {
  Log.debug("deleteUserController :: %o", {
    body: req.body,
    params: req.params,
  });

  const data = await deleteOneUser(req.params.id);

  return successRespond(req, res, SHORT_MSG.successful, 202, data);
};

export const oneUserController = async (req, res) => {
  Log.debug("oneUserController :: %o", { body: req.body, params: req.params });
  const { success, data } = await oneUser({
    value: req.params.value || "",
  });
  if (!success) {
    return errorRespond(res, SHORT_MSG.unexpected_error, 500);
  }
  return successRespond(
    req,
    res,
    SHORT_MSG.successful,
    202,
    transformUserData(data)
  );
};

export const userListController = async (req, res) => {
  Log.debug("userListController :: %o", { body: req.body });
  const { success, data } = await allUsers();
  if (!success) {
    return errorRespond(res, SHORT_MSG.unexpected_error, 500);
  }
  return successRespond(
    req,
    res,
    SHORT_MSG.successful,
    202,
    mapUserTransform(data)
  );
};

export const userOnlyListController = async (req, res) => {
  Log.debug("userOnlyListController :: %o", {
    body: req.body,
    params: req.params,
  });
  const where = {};
  if (req.params?.id) {
    where._id = req.params.value;
  }
  const { success, data } = await allUsersOnly();
  if (!success) {
    return errorRespond(res, SHORT_MSG.unexpected_error, 500);
  }
  return successRespond(req, res, SHORT_MSG.successful, 202, data);
};

export const userOnlyOneListController = async (req, res) => {
  Log.debug("userOnlyListController :: %o", {
    body: req.body,
    params: req.params,
  });
  const where = {};
  if (req.params?.id) {
    where._id = req.params.id;
  }
  const { success, data } = await oneUser(where);
  if (!success) {
    return errorRespond(res, SHORT_MSG.unexpected_error, 500);
  }
  return successRespond(
    req,
    res,
    SHORT_MSG.successful,
    202,
    transformUserData(data)
  );
};

export const memberListController = async (req, res) => {
  Log.debug("memberListController :: %o", { body: req.body });
  const { success, data } = await allUsers({
    role: "member",
  });
  if (!success) {
    return errorRespond(res, SHORT_MSG.unexpected_error, 500);
  }
  return successRespond(req, res, SHORT_MSG.successful, 202, data);
};

export const vendorListController = async (req, res) => {
  Log.debug("vendorListController :: %o", { body: req.body });
  const { success, data } = await allUsers({
    role: "vendor",
  });
  if (!success) {
    return errorRespond(res, SHORT_MSG.unexpected_error, 500);
  }
  return successRespond(req, res, SHORT_MSG.successful, 202, data);
};

export const name = "User";
