import Log from '../utils/logger';
import {
  COMMANDS_KEY,
  AVAILABLE_COMMANDS,
} from '../utils/commands';
import {
  deleteTransaction,
  addTransaction,
} from '../service/transaction';

const handleDiscordActionCommands = async (commandName, amount, from, to, notes, transactionId) => {
  Log.debug('handleDiscordActionCommands :: %o', {
    commandName,
    amount,
    from,
    to,
    notes,
    transactionId,
  });
  if (commandName === COMMANDS_KEY.delete_transaction && transactionId) {
    const {
      success,
    } = await deleteTransaction(transactionId);
    return success ? 'Transaction has been deleted' : 'Transaction delete was unsuccessful';
  }

  if (AVAILABLE_COMMANDS.includes(commandName)) {
    const {
      success,
      data,
      msg,
    } = await addTransaction(commandName, amount, from, to, notes);
    if (msg && msg !== '') {
      return msg;
    }
    return success ? `Transaction created! ID: ${data._id}` : 'Transaction creation was unsuccessful';
  }

  return 'Unexpected error: Unable to handle the transaction data';
};

export default handleDiscordActionCommands;
