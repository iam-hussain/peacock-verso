import Log from '../utils/logger';
import { successRespond, errorRespond, SHORT_MSG } from '../utils/responder';
import { allTreasuries, generateTreasuryList } from '../service/treasury';
import { transformClubTreasury } from '../utils/transform';
import { cleanCatch } from '../service/catcher';

export const cleanCatchController = async (req, res) => {
  Log.debug('cleanCatchController :: %o', { body: req.body });
  const catcherData = await cleanCatch();
  return successRespond(req, res, SHORT_MSG.successful, 202, { catcherData });
};

export const treasuryListController = async (req, res) => {
  Log.debug('treasuryListController :: %o', { body: req.body });
  const { success, data } = await allTreasuries();
  if (!success) {
    return errorRespond(res, SHORT_MSG.unexpected_error, 500);
  }
  return successRespond(req, res, SHORT_MSG.successful, 202, transformClubTreasury(data));
};

export const generateTreasuryController = async (req, res) => {
  Log.debug('treasuryListController :: %o', { body: req.body });
  const { success } = await generateTreasuryList();
  if (!success) {
    return errorRespond(res, SHORT_MSG.unexpected_error, 500);
  }
  return successRespond(req, res, SHORT_MSG.successful, 202, {});
};

export const name = 'Treasury';
