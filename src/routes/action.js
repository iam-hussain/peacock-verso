import { Router } from "express";
import { doBackup, restore } from "../service/db-handler";
import { successRespond, errorRespond, SHORT_MSG } from "../utils/responder";
import { getAllData } from "../service/sync";
import request from "request";

const router = Router();

router.get("/backup", (req, res) =>
  doBackup()
    .then(() => successRespond(req, res, SHORT_MSG.successful, 200, {}))
    .catch(() => errorRespond(res, SHORT_MSG.unsuccessful, 500, {}))
);

router.get("/restore", (req, res) =>
  restore()
    .then(() => successRespond(req, res, SHORT_MSG.successful, 200, {}))
    .catch(() => errorRespond(res, SHORT_MSG.unsuccessful, 500, {}))
);

router.get("/sync", (req, res) =>
  getAllData()
    .then(async (data) => {
      // try {
      //   return request
      //     .post({
      //       url: "http://localhost:3000/api/sync",
      //       formData: data,
      //     })
      //     .then(() => {
      //       return successRespond(req, res, SHORT_MSG.successful, 200, data);
      //     });
      //   //   await fetch("http://localhost:3000/api/sync", {
      //   //     method: "POST",
      //   //     body: data,
      //   //   });
      //   // successRespond(req, res, SHORT_MSG.successful, 200, data);
      // } catch (err) {
      //   console.error(err);
      // }
      return successRespond(req, res, SHORT_MSG.successful, 200, data);
    })
    .catch(() => errorRespond(res, SHORT_MSG.unsuccessful, 33, {}))
);

export default router;
