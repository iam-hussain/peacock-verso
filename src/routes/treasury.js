import { Router } from 'express';
import { treasuryListController, generateTreasuryController, cleanCatchController } from '../controllers/treasuryController';

const router = Router();

router.get('/', treasuryListController);
router.get('/generate', generateTreasuryController);
router.get('/cleanCatch', cleanCatchController);

export default router;
