import { Router } from "express";
import {
  oneUserController,
  userListController,
  userOnlyListController,
  userOnlyOneListController,
  memberListController,
  vendorListController,
  deleteUserController,
} from "../controllers/userController";

const router = Router();

router.get("/", userListController);
router.get("/only/:id", userOnlyOneListController);
router.get("/only", userOnlyListController);
router.get("/member", memberListController);
router.get("/vendor", vendorListController);
router.get("/details/:value", oneUserController);
router.post("/delete/:id", deleteUserController);

export default router;
