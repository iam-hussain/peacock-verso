import { Client, Intents } from 'discord.js';

import Log from '../utils/logger';
import { DISCORD_BOT_TOKEN, DISCORD_GUILD } from './Configs';
import { handleBotMessage, handleInteraction } from '../utils/bot';
import Commands from '../utils/commands';

const loadDiscordClient = () => {
  Log.info('DISCORD_CLIENT :: Connecting to the discard server');
  const client = new Client({
    intents: [
      Intents.FLAGS.GUILDS,
      Intents.FLAGS.DIRECT_MESSAGES,
      Intents.FLAGS.GUILD_MESSAGES,
    ],
  });

  client.on('ready', () => {
    Log.info(`DISCORD_CLIENT ::  Connected to the ${client.user.tag} server`);

    let commands = client.application?.commands;
    if (DISCORD_GUILD) {
      const guild = client.guilds.cache.get(DISCORD_GUILD);
      if (guild) {
        commands = guild.commands;
      }
    }
    if (commands) {
      Commands.map((each) => commands.create(each));
    }
  });

  client.on('message', (message) => handleBotMessage(message));

  client.on('interactionCreate', async (interaction) => {
    if (!interaction.isCommand()) return;
    await handleInteraction(interaction);
  });

  client.login(DISCORD_BOT_TOKEN);
};

export default loadDiscordClient;
