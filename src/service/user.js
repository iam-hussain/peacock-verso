import User from "../models/User";
import Transactions from "../models/Transactions";
import Treasury from "../models/Treasury";
import Log from "../utils/logger";

export const allUsersOnly = async (where = {}) => {
  try {
    const userList = await User.find(where).lean().exec();
    return {
      success: true,
      data: userList,
    };
  } catch (_err) {
    Log.error("allUsersOnly :: ", _err);
    return {
      success: false,
      data: [],
    };
  }
};

export const deleteOneUser = async (id) => {
  try {
    const user = await User.deleteMany({
      _id: id,
    });

    const transactionFrom = await Transactions.deleteMany({
      from: id,
    });

    const transactionTo = await Transactions.deleteMany({
      to: id,
    });

    const treasury = await Treasury.deleteMany({
      user: id,
    });

    Log.debug("deleteOneUser :: %o", {
      user,
      transactionFrom,
      transactionTo,
      treasury,
    });

    return {
      success: true,
      data: {
        user,
        transactionFrom,
        transactionTo,
        treasury,
      },
    };
  } catch (_err) {
    Log.error("deleteOneUser :: ", _err);
    return {
      success: false,
    };
  }
};

export const oneUser = async (where = {}) => {
  try {
    const userList = await User.findOne(where)
      .lean()
      .populate("treasury")
      .exec();

    if (!userList) {
      return {
        success: false,
        data: {},
      };
    }
    return {
      success: true,
      data: userList,
    };
  } catch (_err) {
    Log.error("oneUser :: ", _err);
    return {
      success: false,
      data: {},
    };
  }
};

export const allUsers = async (where = {}) => {
  try {
    const userList = await User.find(where)
      .lean()
      .populate("treasury")
      .populate("transaction_in")
      .populate("transaction_out")
      .populate("deposit_count")
      .exec();
    return {
      success: true,
      data: userList,
    };
  } catch (_err) {
    Log.error("allUsers :: ", _err);
    return {
      success: false,
      data: [],
    };
  }
};

export const getFromTo = async (from, to) => {
  const where = {};

  if (from && to) {
    where.value = [from, to];
  } else if (from && !to) {
    where.value = from;
  } else if (!from && to) {
    where.value = to;
  } else {
    return {
      success: false,
      data: {},
    };
  }
  Log.debug("getFromTo :: where :: %o", where);

  try {
    const userList = await User.find(where).populate("treasury").lean().exec();

    if (!userList || userList.length === 0) {
      return {
        success: false,
        data: {},
      };
    }

    const data = {};

    if (from) {
      data.fromUser = userList.find((each) => each.value === from);
      if (!data.fromUser) {
        return {
          success: false,
          data: {},
        };
      }
    }

    if (to) {
      data.toUser = userList.find((each) => each.value === to);
      if (!data.toUser) {
        return {
          success: false,
          data: {},
        };
      }
    }

    return {
      success: true,
      data,
    };
  } catch (_err) {
    Log.error("allTransaction :: ", _err);
    return {
      success: false,
      data: [],
    };
  }
};
