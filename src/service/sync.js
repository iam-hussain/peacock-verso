import User from "../models/User";
import Transactions from "../models/Transactions";
import Treasury from "../models/Treasury";
import Log from "../utils/logger";

export const getAllData = async () => {
  try {
    const users = await User.find();
    const transactions = await Transactions.find();
    return {
      success: true,
      users,
      transactions,
    };
  } catch (_err) {
    Log.error("getAllData :: ", _err);
    return {
      success: false,
      users: [],
      transactions: [],
    };
  }
};
