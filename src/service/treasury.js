/* eslint-disable max-len */
import Transactions from '../models/Transactions';
import Treasury from '../models/Treasury';
import Log from '../utils/logger';
import { COMMANDS_KEY } from '../utils/commands';

import { clubDetails } from '../providers/Constants';

const removeUndefined = (object) => JSON.parse(JSON.stringify(object));

export const rawTransactions = async () => {
  try {
    return await Transactions.find({ deleted: false }).lean().exec() || [];
  } catch (_err) {
    Log.error('rawTransactions :: ', _err);
    return [];
  }
};

export const rawTreasuries = async () => {
  try {
    return await Treasury.find({}).lean().exec() || [];
  } catch (_err) {
    Log.error('rawTreasuries :: ', _err);
    return [];
  }
};

export const allTreasuries = async () => {
  try {
    const treasuryList = await Treasury.find({}).populate('user').sort({ investment: -1 }).lean()
      .exec();
    return {
      success: true,
      data: treasuryList,
    };
  } catch (_err) {
    Log.error('allTreasuries :: ', _err);
    return {
      success: false,
      data: [],
    };
  }
};

export const generateTreasuryList = async () => {
  try {
    const objectTreasuries = await rawTreasuries();
    const transactions = await rawTransactions();

    const treasuries = [];

    // eslint-disable-next-line array-callback-return
    objectTreasuries.map(({ _id, user, value }) => {
      treasuries.push({
        _id,
        value,
        holding_amount: 0,
        spent_amount: 0,
        investment: 0,
        investment_return: 0,
        received: 0,
        number_of_paid_months: 0,
        user,
      });
    });

    const clubIndex = treasuries.findIndex((each) => each.value === clubDetails.value);

    // Log.debug('generateTreasuryList :: treasuries :: %o', { treasuries });

    transactions.map((transaction) => {
      const fromId = transaction?.from.toString() || '';
      const toId = transaction?.to ? transaction?.to.toString() : '';
      const fromIndex = treasuries.findIndex((each) => (each?.user ? each.user.toString() === fromId : false));
      const toIndex = treasuries.findIndex((each) => (each?.user ? each.user.toString() === toId : false));

      // Log.debug('generateTreasuryList :: from/to :: %o', { transaction, fromId, toId, fromIndex, toIndex });
      if (fromIndex >= 0) {
        if (transaction.method === COMMANDS_KEY.transfer) {
          treasuries[fromIndex].holding_amount -= transaction.amount;

          treasuries[toIndex].holding_amount += transaction.amount;
        }

        if (transaction.method === COMMANDS_KEY.deposit) {
          treasuries[fromIndex].investment += transaction.amount;
          treasuries[fromIndex].number_of_paid_months += Math.floor(transaction.amount / 1000);

          treasuries[toIndex].holding_amount += transaction.amount;

          treasuries[clubIndex].received += transaction.amount;
          treasuries[clubIndex].holding_amount += transaction.amount;
        }

        if (transaction.method === COMMANDS_KEY.withdraw) {
          treasuries[fromIndex].holding_amount -= transaction.amount;

          treasuries[toIndex].investment -= transaction.amount;
          treasuries[toIndex].number_of_paid_months -= Math.floor(+(transaction.amount / 1000));

          treasuries[clubIndex].received -= transaction.amount;
          treasuries[clubIndex].holding_amount -= transaction.amount;
        }

        if (transaction.method === COMMANDS_KEY.expenditure) {
          treasuries[fromIndex].holding_amount -= transaction.amount;

          treasuries[clubIndex].holding_amount -= transaction.amount;
          treasuries[clubIndex].spent_amount += transaction.amount;
        }

        if (transaction.method === COMMANDS_KEY.invest) {
          treasuries[fromIndex].holding_amount -= transaction.amount;

          treasuries[toIndex].investment += transaction.amount;
          treasuries[toIndex].number_of_paid_months += 1;

          treasuries[clubIndex].holding_amount -= transaction.amount;
          treasuries[clubIndex].investment += transaction.amount;
        }

        if (transaction.method === COMMANDS_KEY.return_on_invest) {
          treasuries[fromIndex].investment_return += transaction.amount;

          treasuries[toIndex].holding_amount += transaction.amount;

          treasuries[clubIndex].holding_amount += transaction.amount;
          treasuries[clubIndex].investment_return += transaction.amount;
          // eslint-disable-next-line max-len
        }
      }

      return transaction;
    });

    // Log.debug('generateTreasuryList :: transaction :: %o', { treasuries });
    const treasuriesMapper = treasuries.map(async (each) => {
      await Treasury.findOneAndUpdate(
        { _id: each._id }, removeUndefined({
          ...each,
          _id: undefined,
          user: undefined,
          value: undefined,
        }),
      );
    });
    await Promise.allSettled(treasuriesMapper);
    return {
      success: true,
      data: [],
    };
  } catch (_err) {
    Log.error('generateTreasuryList :: ', _err);
    return {
      success: false,
      data: [],
    };
  }
};
