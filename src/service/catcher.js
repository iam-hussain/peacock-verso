import Log from '../utils/logger';
import redisClient from '../providers/Redis';
import { ENABLE_REDIS } from '../providers/Configs';

const keyStoryKey = 'KEY_STORE';
const generateKey = (originalUrl) => `GET_API_CALL:${originalUrl}`;

const setKeyStore = async (key) => {
  const keyData = await redisClient.get(keyStoryKey);
  Log.debug(`REDIS :: setKeyStore :: ${key}`);
  if (!keyData) {
    await redisClient.set(keyStoryKey, JSON.stringify([key]));
    return;
  }
  const storedKeys = JSON.parse(keyData) || [];
  await redisClient.set(keyStoryKey, JSON.stringify([...storedKeys, key]));
};

export const setCatch = async (originalUrl, data) => {
  if (!ENABLE_REDIS) {
    return;
  }
  const key = generateKey(originalUrl);
  Log.debug(`REDIS :: setCatch :: ${key}`);
  await setKeyStore(key);
  await redisClient.set(key, JSON.stringify(data));
};

export const getCatch = async (originalUrl) => {
  if (!ENABLE_REDIS) {
    return {
      isCached: false,
      cachedData: {},
    };
  }
  const key = generateKey(originalUrl);
  const cachedData = await redisClient.get(key);
  Log.debug(`REDIS :: getCatch :: ${key} :: ${!!cachedData}`);
  if (!cachedData) {
    return {
      isCached: false,
      cachedData: {},
    };
  }
  return {
    isCached: true,
    cachedData: JSON.parse(cachedData),
  };
};

export const deleteCatch = async (originalUrl) => {
  if (!ENABLE_REDIS) {
    return;
  }
  const key = generateKey(originalUrl);
  Log.debug(`REDIS :: deleteCatch :: ${key}`);
  await redisClient.del(key);
};

export const cleanCatch = async () => {
  if (!ENABLE_REDIS) {
    return {};
  }
  const keyData = await redisClient.get(keyStoryKey);
  const storedKeys = JSON.parse(keyData) || [];
  Log.debug(`REDIS :: cleanCatch :: ${storedKeys.join(', ')}`);
  const mapper = storedKeys.map((e) => redisClient.del(e));
  await Promise.all(mapper);
  await redisClient.del(keyStoryKey);
  return {
    mapper,
    previouslyStoredKeys: storedKeys,
    keyStoryKey,
  };
};
