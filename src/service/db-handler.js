import path from "path";
import { spawn, exec } from "child_process";
import { MONGO_DB_URL, DB_BACKUP_PATH } from "../providers/Configs";
import Log from "../utils/logger";

const BACKUP_PATH = DB_BACKUP_PATH || path.resolve(__dirname, "../../backups");
const resolvedPath = path.resolve(BACKUP_PATH, "backup");

export function backup(resolve, reject) {
  Log.info("DB BACKUP :: Starting....", BACKUP_PATH);
  const child = spawn("mongodump", [
    `--uri=${MONGO_DB_URL}`,
    `--archive=${resolvedPath}.gzip`,
    "--gzip",
    "--drop",
    "-vvvvvv",
  ]);
  child.stdout.on("data", (data) => {
    Log.debug("DB BACKUP :: stdout :: %o", data);
  });
  child.stderr.on("data", (data) => {
    Log.debug("DB BACKUP :: stderr :: %o", Buffer.from(data).toString());
  });
  child.on("error", (err) => {
    Log.error("DB BACKUP :: error", err);
    return reject();
  });
  child.on("exit", (code, signal) => {
    if (code) {
      Log.info("DB BACKUP :: process exit with code :: %o", code);
      return reject();
    }
    if (signal) {
      Log.info("DB BACKUP :: process killed with signal :: %o", signal);
      return reject();
    }
    Log.info("DB BACKUP :: BACKUP success");
    return resolve();
  });
}

export function doBackup() {
  return new Promise((resolve, reject) => {
    const command = `mv ${resolvedPath}.gzip ${resolvedPath}_old.gzip`;
    Log.debug(command);
    exec(command, backup(resolve, reject));
  });
}

export function restore() {
  return new Promise((resolve, reject) => {
    Log.info("DB RESTORE :: Starting....");
    console.log({ MONGO_DB_URL, resolvedPath });
    const child = spawn("mongorestore", [
      `--uri=${MONGO_DB_URL}`,
      `--archive=${resolvedPath}.gzip`,
      "--gzip",
      "--drop",
      "-vvvvvv",
    ]);
    child.stdout.on("data", (data) => {
      Log.debug("DB RESTORE :: stdout :: %o", data);
    });
    child.stderr.on("data", (data) => {
      Log.debug("DB RESTORE :: stderr :: %o", Buffer.from(data).toString());
    });
    child.on("error", (err) => {
      Log.error("DB RESTORE :: error", err);
      return reject();
    });
    child.on("exit", (code, signal) => {
      if (code) {
        Log.info("DB RESTORE :: process exit with code :: %o", code);
        return reject();
      }
      if (signal) {
        Log.info("DB RESTORE :: process killed with signal :: %o", signal);
        return reject();
      }
      Log.info("DB RESTORE :: RESTORE success");
      return resolve();
    });
  });
}
